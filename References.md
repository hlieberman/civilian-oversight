Some useful references
======================

- [Massachusetts Legislative Drafting Manual](https://malegislature.gov/Legislation/DraftingManual)

Special Investigations Unit, Ontario
------------------------------------
- [Unit Mission](www.siu.on.ca/en/mission.php)
- [Unit Organization](http://www.siu.on.ca/en/org_chart.php)
- [Police Services Act](http://www.siu.on.ca/en/psa.php)
- [Regulation 267/10](http://www.siu.on.ca/en/onr-267.php)
