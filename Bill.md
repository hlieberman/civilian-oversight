TITLE:  An act establishing the independent investigations bureau

SECTION 1.  Chapter 12 of the General Laws is hereby amended by inserting after section 11 the following section:-

(a) As used in this section the following words shall, unless the context clearly requires otherwise, have the following meanings:

"Bureau", the independent investigation bureau.

"Director", the director of the independent investigation bureau, or the acting director in the event of the director's absence or inability to perform their duties.

"Immediate relative", the spouse, children or parents of a person.

"Police officer", any sworn law enforcement officer operating within the bounds of the Commonwealth, including sheriffs and their deputies appointed or elected under the terms of Chapter 37, state police officers appointed under Chapter 22C, city or town police appointed under Chapter 41 or any other person with constabulary powers, provided that this definition shall not encompass anyone who possesses constabulary powers which are limited to only serving and executing civil process.

(b) There shall be in the department of the attorney general an independent investigation bureau.  The attorney general shall designate a director of the independent investigation bureau, subject to the restrictions in subsection c.

(c) No person shall be appointed as the director who has been or currently is a police officer, nor shall any person be appointed as the director who has an immediate relative who is currently serving as a police officer.

(d) The director may designate an acting director to fulfill the responsibilities of the director in the event of the absence or inability of the director to perform their responsibilities, provided that any person so designated must comply with the same restrictions in subsection c.

(e) The director may appoint and remove investigative, legal, expert or clerical staff as the work of the bureau may require, provided that no person so appointed shall concurrently be a police officer.

(f) Investigators of the bureau shall be peace officers.  The director shall not assigned an investigator to any investigation involving a department that the investigator previously served at.

(g) The director may, at their own initiative, or shall, at the request of the attorney general, open an investigation to be conducted into the circumstances of serious injuries or deaths that may have resulted from criminal offenses committed by police officers in the Commonwealth of Massachusetts.

(h) The chief of police or other supervising officer shall contact the director of the independent investigation bureau immediately after any incident involving their officers which may be under the jurisdiction of the bureau.  The same shall ensure, pending the bureau taking control of the scene, that the scene is secured in a manner consistent with the usual practice of the police force for serious incidents.  The same shall, to the extent that it is practical, segregate all police officers involved in an incident from each other until after the bureau has completed its interviews.

(i) No police officer involved or witnessing any incident under the jurisdiction of the bureau shall communicate with any other officer involved or witnessing the same concerning the incident until after the bureau has completed its interviews.

(j) In any incident investigated by the bureau, the bureau shall be the primary investigatory agency.  Nothing in this section shall prevent any other police force from investigating any incident involving their officers, provided that the bureau shall have priority over other concurrent investigations, and that no involved police officer shall be interviewed until after the bureau has completed its interviews.

(k) Upon the completion of the investigation of an incident, the director shall submit a report to the attorney general detailing its findings.  If the director finds that there is probable cause that a crime was committed, the director shall arrange for criminal charges to be brought.

(l) Prosecution of police officers charged as a result of a finding by the director of probable cause that a crime was committed shall be performed by the public integrity division of the attorney general's office.

(m) The director shall make its resources available to the public integrity division in the event that the public integrity division requires assistance in the prosecution of a case brought under the terms of this section.

(n) The public integrity division shall not use any staff in the prosecution of a case brought under the terms of this section who are currently serving as police officers.