Increasing Public Trust in Police Incidents
===========================================

The purpose of this repository is to be a working space for the
drafting of a bill to establish an investigatory body charged with the
investigation of police-involved deaths, serious injuries, or other
serious incidents of harm.

Specifics
---------
This repository is primarily focused on drafting a bill for the
Commonwealth of Massachusetts, however, many of the components may be
able to be reused by those interested in drafting similar bills for
different jurisdictions.  Wherever possible, the outlines for the unit
will be general enough to be reused directly; the bill itself will not
be.  This legislation is heavily inspired by the Special Investigations Unit of the Province of Ontario.

License
-------
All files in this repository are licensed under the Creative Commons
Attribution-ShareAlike 4.0 International license, otherwise known as
CC BY-SA 4.0.

For more information, see the [Creative Commons human-readable
description](https://creativecommons.org/licenses/by-sa/4.0/), or the
legal code itself in COPYING.

Contributor License Agreements are requested for any contributions, as
I am not sure whether or not copyright will have to be transferred to
the Commonwealth before submission.  If you have objections to this,
please reach out to me, and I will try and get an answer about whether
this is necessary from the House Counsel.

A note on police conduct
------------------------
I have been asked why this bill is necessary in Massachusetts, and
been asked to point to examples of police misconduct.  I want to be
clear, I strongly believe that this bill serves the public whether or
not there is police misconduct in the Commonwealth.  The presence of
an independent third-party investigating issues of police-involved
incidents enables both the public and the officers themselves to have
confidence that the investigation will be impartial and fair to all
involved.  The unit is intentionally placed outside the command
structure of any police department to ensure that there are no
conflicts of interest.
