General Organizational Outline
==============================

Structure
---------
The UNIT NAME will be organized such that its Director reports
directly to the Attorney General or equivalent.  It should be an
appointed position by the Attorney General, with optional confirmation
by another executive body (c.f. The Governor's Council).

The Director may not be a current or past police officer, nor may any
member of their immediate family (siblings, parents, children, or
spouses) be current police officers.  The Director's contract should
restrict them from becoming a police officer for a certain term after
their departure from the post of Director.

The Director may designate an Acting Director to fulfill their
responsibilities in the event of their absence or inability to perform
their job, provided that the Acting Director shall not be a current or
past police officer, and no member of their immediate family (as
defined above) shall be current police officers.

The Director shall obtain staff as necessary to perform the
responsibilities of the department.  No staff may be a police
officer during their employment.  Staff positions shall be civil
service positions.

Authority
---------
Investigators of UNIT NAME shall be peace officers.

Police officers shall cooperate with investigators of UNIT NAME.

The Director will not assign any staff to an investigation involving a
department that staff previously was employed or volunteered at.

Jurisdiction
------------
The Director may, by their own initiative, or shall, at the request of
the Attorney General, open an investigation to be conducted into the
circumstances of serious injuries or deaths that may have resulted
from criminal offenses committed by police officers or sheriffs, whether
active, special, or reserve, and whether on or off duty, in the
Commonwealth of Massachusetts.

Contact and Transfer
--------------------
The chief of police or sheriff shall contact UNIT NAME immediately of
any incident involving their officers which may be under the
jurisdiction of UNIT NAME.

The chief of police or sheriff shall ensure, pending UNIT NAME taking
control of the scene, that the scene is secured in a matter consistent
with the usual practice of the police force for serious incidents.

The chief of police or sheriff shall, to the extent that it is
practical, segregate all police officers involved in the incident from
each other until after UNIT NAME has completed its interviews.

No police officer involved in an incident shall communicate with any
other officer involved in the incident concerning the incident until
after UNIT NAME has completed its interviews.

Primacy
-------
In any incident that UNIT NAME is investigating, it shall be the
primary investigatory agency.  Nothing in this act shall prevent any
other police force from investigating the incident, providing that
UNIT NAME has priority over other concurrent investigations and that
any involved police officers shall not be interviewed until after UNIT
NAME has completed its interviews.

Completion of Investigation
---------------------------
When UNIT NAME completes its investigation to the incident, the
Director shall submit a report to the Attorney General detailing its
findings.  If the Director finds that there is probable cause that a
crime was committed, the Director shall arrange for criminal charges
to be brought by the Public Integrity Division of the Attorney
General's Office.

Prosecution
-----------
Prosecution of police charged as a result of an investigation of UNIT
NAME shall be performed by the Public Integrity Division of the
Attorney General's Office.  UNIT NAME shall make its resources
available to the Public Integrity Division in the event that the
Public Integrity Division requires investigators.  The Public
Integrity Division shall not use any investigators assigned to it who
are currently police officers for any case brought as a direct result
of a UNIT NAME investigation.